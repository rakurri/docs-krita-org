.. meta::
   :description:
        Resource Packs for Krita.

.. metadata-placeholder

   :authors: - Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
             - Scott Petrovic
             - Raghavendra Kamath <raghu@raghukamath.com>
             - Nathan Lovato
   :license: GNU free documentation license 1.3 or later.


.. _resources_page:

=========
Resources
=========


Brush Packs
-----------

.. list-table::

        * - .. figure:: /images/resource_packs/Resources-deevadBrushes.jpg
               :target: https://github.com/Deevad/deevad-krita-brushpresets

               David Revoy

          - .. figure:: /images/resource_packs/Resources-mirandaBrushes.jpg
               :target: https://drive.google.com/open?id=1hrH4xzMRwzV0SBEt2K8faqZ_YUX-AdyJ

               Ramon Miranda

          - .. figure:: /images/resource_packs/Resources-conceptBrushes.jpg
               :target: https://forum.kde.org/viewtopic.php?f=274&t=127423

               Concept art & Illustration Pack
        * - .. figure:: /images/resource_packs/Resources-aldyBrushes.jpg
               :target: https://www.deviantart.com/al-dy/art/Aldys-Brush-Pack-for-Krita-2-3-1-196128561

               Al-dy

          - .. figure:: /images/resource_packs/Resources-vascoBrushes.jpg
               :target: https://vascobasque.com/modular-brushset/

               Vasco Basqué

          - .. figure:: /images/resource_packs/Resources-stalcryBrushes.jpg
               :target: https://www.deviantart.com/stalcry/art/Krita-Custom-Brushes-350338351

               Stalcry

        * - .. figure:: /images/resource_packs/Resources-woltheraBrushes.jpg
               :target: https://forum.kde.org/viewtopic.php?f=274&t=125125

               Wolthera

          - .. figure:: /images/resource_packs/Resources-nylnook.jpg
               :target: https://nylnook.art/en/blog/krita-brushes-pack-v2/

               Nylnook


          - .. figure:: /images/resource_packs/Resources-raghukamathBrushes.png
               :target: https://gitlab.com/raghukamath/krita-brush-presets/-/releases

               Raghukamath

        * - .. figure:: /images/resource_packs/Resources-GDQuestBrushes.jpeg
               :target: https://github.com/GDquest/free-krita-brushes/releases/

               GDQuest

          - .. figure:: /images/resource_packs/Resources-iForce73Brushes.png
               :target: https://www.deviantart.com/iforce73/art/Environments-2-0-759523252

               IForce73

          - .. figure:: /images/resource_packs/Resources-wojtrybBrushes.png
               :target: https://www.dropbox.com/s/i1rt7f0qc77nc4m/wont_teach_you_to_draw_brushpack_v5.0.zip?dl=1

               wojtryb
        * -

          -
          
          -

Texture Packs
-------------

.. list-table::

        * - .. figure:: /images/resource_packs/Resources-deevadTextures.jpg
               :target: https://www.davidrevoy.com/article156/texture-pack-1

               David Revoy

          - .. figure:: /images/resource_packs/Resources-deevadTextures2.jpg
               :target: https://www.davidrevoy.com/article263/five-traditional-textures

               David Revoy

External tutorials
------------------

.. list-table::

        * - .. figure:: /images/resource_packs/simon_pixel_art_course.png
               :target: https://www.udemy.com/learn-to-create-pixel-art-from-zero/?couponCode=OTHER_75

               Simón Sanchez' "Learn to Create Pixel Art from Zero" course on Udemy

User-made Python Plugins
------------------------
To install and manage your plugins, visit the :ref:`krita_python_plugin_howto` area. See the second area on how to get Krita to recognize your plugin.



Usability
~~~~~~~~~
Direct Eraser Plugin

    https://www.mediafire.com/file/sotzc2keogz0bor/Krita+Direct+Eraser+Plugin.zip

ThreeSlots plugin: creates three brushtool shortcuts that memorize last used brush preset for each slot independently from each other.

    https://github.com/DarkDefender/threeslots

QuickColor: shortcuts to get a color from a specified palette

    https://github.com/JonasLW/QuickColor

BrushColorSwitch: switches brush and foreground/background color with the same action/shortcut.

    https://github.com/rkspsm/BrushColorSwitch

Tablet Controls Docker

    https://github.com/tokyogeometry/tabui

On-screen Canvas Shortcuts

    https://github.com/qeshi/henriks-onscreen-krita-shortcut-buttons/tree/master/henriks_krita_buttons




Workflow improvements
~~~~~~~~~~~~~~~~~~~~~

AnimLayers (Animate with Layers)

    https://github.com/thomaslynge/krita-plugins

Reference Image Docker (old style)

    https://github.com/antoine-roux/krita-plugin-reference
    
Mirror Fix - Correct Symmetry Errors

    https://github.com/EyeOdin/mirror_fix

ToggleRefLayer: enables you to assign a keyboard shortcut to toggle the visibility of a reference layer named "reference"

    https://drive.google.com/file/d/11O8FiejleajsT_uHd4Q4VBrCrYX9Rh5v/view?usp=sharing

Shotgun Toolkit Engine for Krita: allows working in a managed way, loading/saving/publishing artwork, keeping it up to date and publishing your projects and layers into Shotgun

    https://github.com/diegogarciahuerta/tk-krita


File management
~~~~~~~~~~~~~~~~

Art Revision Control (using GIT)

    https://github.com/abeimler/krita-plugin-durra
    
.. GDQuest - Designer Tools
..
..    https://github.com/GDquest/Krita-designer-tools


Spine File Format Export

    https://github.com/chartinger/krita-unofficial-spine-export



Color selectors
~~~~~~~~~~~~~~~

Pigment.O - Color Picker

    https://github.com/EyeOdin/Pigment.O
    
    
Interface
~~~~~~~~~
Subwindow organizer: split screen, adjusting to window size changes, snapping to workspace borders and more.

    https://github.com/wojtryb/kritaSubwindowOrganizer
    
    
KanvasBuddy: a small dialog that floats on top of the canvas packed with enough features to let you spend as much time in Canvas-Only mode as possible. 
The idea behind KB was to provide the 20% of tools used 80% of the time in the most out-of-the-way GUI possible.

    Source, main page and download link: https://github.com/Kapyia/KanvasBuddy
    
    Krita-artists thread: https://krita-artists.org/t/kanvasbuddy-a-minimalist-toolbar/549




Python plugins development
~~~~~~~~~~~~~~~~~~~~~~~~~~
Krita Plugin generator

    https://github.com/cg-cnu/vscode-krita-plugin-generator
    
Python auto-complete for text editors

    https://github.com/scottpetrovic/krita-python-auto-complete
    
    



Miscellaneous
~~~~~~~~~~~~~~~~~~~~~
Timer Watch - Time Management Tool

    https://github.com/EyeOdin/timer_watch

Post images on Mastodon

    https://github.com/spaceottercode/kritatoot

Bash Action (works with OSX and Linux)

    https://github.com/juancarlospaco/krita-plugin-bashactions#krita-plugin-bashactions




See Something We Missed?
------------------------
Have a resource you made and want to to share it with other artists? Let us know in the forum or visit our chat room to discuss getting the resource added to here.

.. note:: We have curated a list of community created resources for Krita. These resources will be hosted on external website, which is not under the control of Krita or KDE. Please report any error or corrections in the content to the Krita developers.
