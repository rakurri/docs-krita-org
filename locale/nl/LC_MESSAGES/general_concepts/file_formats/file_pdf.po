# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:05+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../general_concepts/file_formats/file_pdf.rst:1
msgid "The PDF file format in Krita."
msgstr "Het bestandsformaat PDF in Krita."

#: ../../general_concepts/file_formats/file_pdf.rst:10
msgid "*.pdf"
msgstr "*.pdf"

#: ../../general_concepts/file_formats/file_pdf.rst:10
msgid "PDF"
msgstr "PDF"

#: ../../general_concepts/file_formats/file_pdf.rst:15
msgid "\\*.pdf"
msgstr "\\*.pdf"

#: ../../general_concepts/file_formats/file_pdf.rst:17
msgid ""
"``.pdf`` is a format intended for making sure a document looks the same on "
"all computers. It became popular because it allows the creator to make sure "
"that the document looks the same and cannot be changed by viewers. These "
"days it is an open standard and there is quite a variety of programs that "
"can read and save PDFs."
msgstr ""
".pdf is een formaat bedoeld voor het maken van een document dat er hetzelfde "
"uit ziet op alle computers. Het werd populair omdat het de maker in staat "
"stelt om er zeker van te zijn dat het document er hetzelfde uitziet en niet "
"gewijzigd kan worden door de kijkers. Tegenwoordig is het een open standaard "
"en er is een variëteit van programma's die PDF's kunnen lezen en opslaan."

#: ../../general_concepts/file_formats/file_pdf.rst:19
msgid ""
"Krita can open PDFs with multiple layers. There is currently no PDF export, "
"nor is that planned. If you want to create a PDF with images from Krita, use "
"`Scribus <https://www.scribus.net/>`_."
msgstr ""
"Krita kan PDF's met meerdere lagen openen. Er is op dit moment geen "
"exporteren naar PDF, noch is dat gepland. Als u een PDF met afbeeldingen uit "
"Krita wilt maken, gebruik dan `Scribus <https://www.scribus.net/>`_."

#: ../../general_concepts/file_formats/file_pdf.rst:21
msgid ""
"While PDFs can be viewed via most browsers, they can also become very heavy "
"and are thus not recommended outside of official documents. Printhouses will "
"often accept PDF."
msgstr ""
"Terwijl PDF's bekeken kunnen worden via de meeste browsers, kunnen ze ook "
"zeer zwaar worden en worden dus niet aanbevolen buiten officiële documenten. "
"Drukkerijen zullen vaak PDF accepteren."
