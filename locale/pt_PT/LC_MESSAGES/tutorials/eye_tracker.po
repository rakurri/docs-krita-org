# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-14 10:25+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita image eyetrackerlayoutscreenshot Uyar Turgut\n"
"X-POFile-SpellExtra: images alt set Min krita DD blank emos YY HH Program\n"
"X-POFile-SpellExtra: in kritarc datacompleta cloud KritaFAQ copy Get\n"
"X-POFile-SpellExtra: delims start wmic ref bin LockAllDockerPanels form\n"
"X-POFile-SpellExtra: tokens dt echo true Users YYYY exe off value Sec\n"
"X-POFile-SpellExtra: localdatetime kra\n"

#: ../../tutorials/eye_tracker.rst:None
msgid ""
".. image:: images/eyetracker_layout_screenshot.png\n"
"   :alt: Screenshot of Krita when used with an eye tracker."
msgstr ""
".. image:: images/eyetracker_layout_screenshot.png\n"
"   :alt: Imagem do Krita quando é usado com um sistema de seguimento dos "
"olhos."

#: ../../tutorials/eye_tracker.rst:1
msgid "Setting up Krita to use with an eye tracker. "
msgstr ""
"Configuração do Krita para o usar com um aparelho de seguimento dos olhos. "

#: ../../tutorials/eye_tracker.rst:1
msgid "- H. Turgut Uyar <hturgut@uyar.info> "
msgstr "- H. Turgut Uyar <hturgut@uyar.info> "

#: ../../tutorials/eye_tracker.rst:1
msgid "GNU free documentation license 1.3 or later."
msgstr "Licença GNU de documentação livre na versão 1.3 ou posterior."

#: ../../tutorials/eye_tracker.rst:10
msgid "Eye Tracker"
msgstr "Seguimento de Olhos"

#: ../../tutorials/eye_tracker.rst:10
msgid "User Interface"
msgstr "Interface do Utilizador"

#: ../../tutorials/eye_tracker.rst:10
msgid "Accessibility"
msgstr "Acessibilidade"

#: ../../tutorials/eye_tracker.rst:10
msgid "Physical Disability"
msgstr "Incapacidade Física"

#: ../../tutorials/eye_tracker.rst:17
msgid "An Example Setup for Using Krita with an Eye Tracker"
msgstr ""
"Uma Configuração de Exemplo de Uso do Krita com um Aparelho de Seguimento de "
"Olhos"

#: ../../tutorials/eye_tracker.rst:21
msgid ""
"This is not a reference document. It is based on the experiences of only one "
"user. The information might not be as applicable when using different eye "
"tracker devices or different control software."
msgstr ""
"Este não é um documento de referência. Baseia-se na experiências de apenas "
"um utilizador. A informação poderá não fazer tanto sentido quando se usarem "
"diferentes dispositivos de seguimento dos olhos ou outras aplicações de "
"controlo."

#: ../../tutorials/eye_tracker.rst:25
msgid ""
"Eye tracker devices are becoming more affordable and they are finding their "
"way into more computer setups. Although these devices are used by various "
"types of users, we will mainly focus on users who have physical disabilities "
"and can only use their eyes to interact with the computer."
msgstr ""
"Os dispositivos de seguimento de olhos estão a tornar-se mais acessíveis e "
"começam a ter lugar em mais configurações de computadores. Ainda que estes "
"dispositivos sejam usados pelos vários tipos de utilizadores, focar-nos-emos "
"principalmente nos utilizadores que têm incapacidades físicas e que só "
"conseguem usar os olhos para interagir com o computador."

#: ../../tutorials/eye_tracker.rst:30
msgid ""
"If you don't already have experience with such a case, here are a few things "
"you'll need to know before you start:"
msgstr ""
"Se não se tiver já deparado com um destes casos, existem algumas coisas que "
"precisa de saber antes de mais nada:"

#: ../../tutorials/eye_tracker.rst:33
msgid ""
"The eye tracker needs to be properly calibrated such that the pointer will "
"be very close to the point where the user is looking at. This might be "
"difficult to achieve, especially if the positioning of the eye tracker with "
"respect to the user can not be fixed between different sessions."
msgstr ""
"O sistema de seguimento de olhos precisa de ser calibrado adequadamente de "
"forma que o cursor fique muito próximo do ponto para onde o utilizador está "
"a olhar. Isto poderá ser difícil de conseguir, especialmente se o "
"posicionamento do dispositivo em relação ao utilizador não conseguir ficar "
"fixo entre diferentes sessões."

#: ../../tutorials/eye_tracker.rst:38
msgid ""
"The lack of accuracy in control makes it nearly impossible to hit small "
"areas on the screen such as small buttons or menu items. Corners and edges "
"of the screen might be difficult to reach too. You also don't want to put "
"interface elements close to one another since it increases the chances of "
"selecting the wrong element accidentally."
msgstr ""
"A falta de precisão no controlo torna quase impossível aceder a pequenas "
"áreas no ecrã, como botões ou itens de menu pequenos. Os cantos e extremos "
"do ecrã poderão também ser difíceis de aceder. Também não irá querer ter os "
"elementos da interface próximos uns dos outros, dado que isto aumenta as "
"hipóteses de seleccionar o elemento errado por acaso."

#: ../../tutorials/eye_tracker.rst:44
msgid ""
"Mouse operations like single click, double click, right click, drag and "
"drop, etc. all demand extra effort in the form of switching modes in the "
"program that controls the device. You will want to keep these switches to a "
"minimum so that the work will not be interrupted frequently."
msgstr ""
"As operações do rato, como o 'click' simples, o duplo, o do botão direito, o "
"arrastamento, etc., necessitam todos de algum esforço extra na forma de "
"mudança de modos no programa que controla o dispositivo. Irá querer reduzir "
"ao máximo estas opções para que o trabalho não seja interrompido com "
"frequência."

#: ../../tutorials/eye_tracker.rst:49
msgid ""
"Switching the mode doesn't automatically start the operation. You need an "
"extra action for that. In our case, this action is \"dwelling\". For "
"example, to start a program, you switch to the left double click mode and "
"then dwell on the icon for the application to activate the double click. "
"Adjusting the dwell time is an important tradeoff: shorter dwell times allow "
"for faster work but are also more error-prone."
msgstr ""
"A mudança de modo não inicia automaticamente a operação. Será necessária uma "
"acção extra para tal. No nosso caso, esta acção é o \"deslocamento\". Por "
"exemplo, para iniciar um programa, irá mudar para o modo de duplo-click do "
"botão esquerdo e depois deslocar-se para o ícone da aplicação, para activar "
"o duplo-click. O ajuste do tempo de deslocamento é um compromisso "
"importante: tempos de deslocamento mais curtos permitem um trabalho mais "
"rápido, mas também mais sujeito a erros."

#: ../../tutorials/eye_tracker.rst:58
msgid "Requirements"
msgstr "Requisitos"

#: ../../tutorials/eye_tracker.rst:60
msgid ""
"Besides the *obvious* requirement of having an eye tracker device, you will "
"also need a control program that will let you interact with the device. When "
"you obtain the device, such a program will most probably be provided to you "
"but that program might not be sufficient for using the device with Krita."
msgstr ""
"Para além do requisito *óbvio* de ter um dispositivo de seguimento dos "
"olhos, também irá precisar de um programa de controlo que lhe permita "
"interagir com o dispositivo. Quando conseguir arranjar esse dispositivo, um "
"desses programas ser-lhe-á oferecido, porém esse programa poderá não ser "
"suficiente para usar o dispositivo com o Krita."

#: ../../tutorials/eye_tracker.rst:65
msgid ""
"One of the basic functionalities of these programs is to emulate mouse "
"clicks. In our case, the program provides a hovering menu which includes "
"large buttons for switching modes between left/right mouse buttons and "
"single/double clicks. After selecting the mode, the hovering menu can be "
"collapsed so that it will leave more screen space for the application."
msgstr ""
"Uma das funcionalidades básicas destes programas é emular os 'clicks' do "
"rato. No nosso caso, o programa oferece um menu à passagem que inclui botões "
"grandes para mudar de modo entre os botões esquerdo/direito e os 'clicks' "
"simples/duplos. Depois de seleccionar o modo, o menu à passagem poderá ser "
"fechado para que deixe mais espaço livre no ecrã para a aplicação."

#: ../../tutorials/eye_tracker.rst:71
msgid ""
"In order to make them easier to configure and use, some programs include "
"only basic modes like single clicks. This is sufficient for many popular "
"applications like e-mail agents and browsers, but for Krita you need the "
"drag and drop mode to be able to draw. If the provided control software "
"doesn't support this mode (usually called \"mouse emulation\"), you can "
"contact the manufacturer of the device for assistance, or look for open "
"source options."
msgstr ""
"Para os tornar mais fáceis de configurar e usar, alguns programas incluem "
"apenas modos básicos, como os 'clicks' simples. Isto é suficiente para "
"muitas aplicações conhecidas, como os clientes de e-mail e os navegadores, "
"mas para o Krita irá precisar do modo de arrastamento para conseguir "
"desenhar. Se a aplicação de controlo oferecida não suportar este modo "
"(normalmente chamado de \"emulação do rato\"), poderá contactar o fabricante "
"do dispositivo para obter ajuda, ou procurar por alternativas em 'software' "
"livre."

#: ../../tutorials/eye_tracker.rst:80
msgid "Starting Krita"
msgstr "Iniciar o Krita"

#: ../../tutorials/eye_tracker.rst:82
msgid ""
"Basically, setting the control program to left double click mode and "
"dwelling on the Krita icon on the desktop would be enough to start up Krita "
"but there are some issues with this:"
msgstr ""
"Basicamente, a configuração do programa de controlo para o modo de duplo-"
"click do botão esquerdo e de acesso ao ícone do Krita no ecrã seria o "
"suficiente para começar, mas existem alguns problemas com isto:"

#: ../../tutorials/eye_tracker.rst:86
msgid ""
"On startup, Krita asks you to choose a template. It's likely that you don't "
"want to go through this setting every time and just want to start with a "
"blank template."
msgstr ""
"No arranque, o Krita pede-lhe para escolher um modelo. É provável que não "
"queira ver esta configuração todas as vezes e simplesmente queira começar "
"com um modelo em branco."

#: ../../tutorials/eye_tracker.rst:90
msgid ""
"Later, saving the document will require interacting with the file save "
"dialog which is not very friendly for this type of use."
msgstr ""
"Posteriormente, a gravação do documento irá obrigar a interagir com a janela "
"de gravação de ficheiros, o que não é muito amigável para este tipo de "
"utilizações."

#: ../../tutorials/eye_tracker.rst:93
msgid ""
"A workaround for these issues could be creating and saving a blank template "
"and running a script that will copy this template under a new name and send "
"it to Krita. Here's an example script for Windows which uses a timestamp "
"suffix to make sure that each file will have a different name (replace "
"USERNAME with the actual user name)::"
msgstr ""
"Um truque para estes problemas poderia ser a criação e gravação de um modelo "
"em branco e a execução de um programa que irá copiar esse modelo com um novo "
"nome e o seu envio para o Krita. Aqui está um programa de exemplo para o "
"Windows que usa um sufixo com data e hora que se certifica que cada ficheiro "
"terá um diferente nome (substitua o UTILIZADOR pelo seu nome de utilizador "
"verdadeiro)::"

#: ../../tutorials/eye_tracker.rst:99
msgid ""
"@echo off\n"
"for /f \"tokens=2 delims==\" %%a in ('wmic OS Get localdatetime /value') do "
"set \"dt=%%a\"\n"
"set \"YY=%dt:~2,2%\" & set \"YYYY=%dt:~0,4%\" & set \"MM=%dt:~4,2%\" & set "
"\"DD=%dt:~6,2%\"\n"
"set \"HH=%dt:~8,2%\" & set \"Min=%dt:~10,2%\" & set \"Sec=%dt:~12,2%\"\n"
"set \"datestamp=%YYYY%%MM%%DD%\" & set \"timestamp=%HH%%Min%%Sec%\"\n"
"set \"fullstamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%\"\n"
"set filename=USERNAME_%fullstamp%.kra\n"
"copy \"C:\\Users\\USERNAME\\Pictures\\blank.kra\" \"%filename%\"\n"
"start \"C:\\Program Files\\Krita (x64)\\bin\\krita.exe\" \"%filename%\""
msgstr ""
"@echo off\n"
"for /f \"tokens=2 delims==\" %%a in ('wmic OS Get localdatetime /value') do "
"set \"dt=%%a\"\n"
"set \"YY=%dt:~2,2%\" & set \"YYYY=%dt:~0,4%\" & set \"MM=%dt:~4,2%\" & set "
"\"DD=%dt:~6,2%\"\n"
"set \"HH=%dt:~8,2%\" & set \"Min=%dt:~10,2%\" & set \"Sec=%dt:~12,2%\"\n"
"set \"data=%YYYY%%MM%%DD%\" & set \"data=%HH%%Min%%Sec%\"\n"
"set \"datacompleta=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%\"\n"
"set ficheiro=UTILIZADOR_%datacompleta%.kra\n"
"copy \"C:\\Users\\UTILIZADOR\\Imagens\\blank.kra\" \"%ficheiro%\"\n"
"start \"C:\\Program Files\\Krita (x64)\\bin\\krita.exe\" \"%ficheiro%\""

#: ../../tutorials/eye_tracker.rst:109
msgid ""
"Double clicking on this script will create a new Krita file in the same "
"folder as the script file. Since the file already has a name, the file save "
"dialog will be avoided. Combined with autosaving, this can be an efficient "
"way to save your work."
msgstr ""
"Se fizer duplo-click sobre este programa, irá criar um novo ficheiro do "
"Krita na mesma pasta que o ficheiro do programa. Dado que o ficheiro já tem "
"um nome, poderá evitar a janela de gravação de ficheiros. Em conjunto com a "
"gravação automática, esta poderá ser uma forma bastante eficiente de gravar "
"o seu trabalho."

#: ../../tutorials/eye_tracker.rst:116
msgid ""
"Storing these files directly on a cloud storage service will be even safer."
msgstr ""
"Se guardar estes ficheiros directamente num serviço da 'cloud', será ainda "
"mais seguro."

#: ../../tutorials/eye_tracker.rst:118
msgid "You might also deal with some timing issues when starting Krita:"
msgstr "Poderá também lidar com algumas questões de tempos ao iniciar o Krita:"

#: ../../tutorials/eye_tracker.rst:120
msgid ""
"After the icon for Krita or for the script is double clicked and Krita "
"starts loading, lingering on the icon will start a second instance."
msgstr ""
"Depois de ter feito duplo-click no ícone do Krita ou no programa e de o "
"Krita começar a carregar, se tocar no ícone irá iniciar uma segunda "
"instância."

#: ../../tutorials/eye_tracker.rst:123
msgid ""
"Similarly, after double clicking, if another window is accidentally brought "
"to the foreground, Krita might start up partially visible behind that window."
msgstr ""
"De forma semelhante, após um duplo-click, se ficar outra janela em primeiro "
"plano sem querer, o Krita poderá ser iniciado parcialmente visível atrás "
"dessa janela."

#: ../../tutorials/eye_tracker.rst:126
msgid ""
"To prevent these problems, it will help if the users train themselves to "
"look at some harmless spot (like an empty space on the desktop) until Krita "
"is loaded."
msgstr ""
"Para impedir estes problemas, será útil se os utilizadores começarem a "
"treinar para olhar em algum ponto seguro (como um espaço em branco no ecrã) "
"até que o Krita seja carregado."

#: ../../tutorials/eye_tracker.rst:132
msgid "Layout"
msgstr "Disposição"

#: ../../tutorials/eye_tracker.rst:134
msgid ""
"Since the interface elements need to be large, you have to use the screen "
"area economically. Running in full-screen mode and getting rid of unused "
"menus and toolbars are the first steps that you can take. Here's the "
"screenshot of our layout:"
msgstr ""
"Dado que os elementos da interface precisam de ser grandes, poderá ter de "
"usar a área do ecrã de form económica. Executar no modo de ecrã completo e "
"ver-se livre de alguns menus e barras de ferramentas não usados serão os "
"primeiros passos a ter em conta. Aqui está uma imagem da nossa disposição:"

#: ../../tutorials/eye_tracker.rst:144
msgid ""
"You will want to put everything you need somewhere you can easily access. "
"For our drawings, the essential items are brushes and colors. So we've "
"decided to place permanent dockers for these."
msgstr ""
"Irá querer colocar tudo o que precisa em algum ponto onde consiga aceder "
"facilmente. Para os nossos desenhos, os itens essenciais são os pincéis e as "
"cores. Como tal, optámos por colocar áreas permanentes para estes itens."

#: ../../tutorials/eye_tracker.rst:148
msgid ""
"Krita features many brushes but the docker has to contain a limited number "
"of those so that the brush icons can be large enough. We recommend that you "
"create :ref:`a custom brush preset to your own liking "
"<loading_saving_brushes>`."
msgstr ""
"O Krita possui muitos pincéis mas a área acoplável tem de conter apenas um "
"número limitado deles, para que os ícones dos pincéis possam ser grandes o "
"suficiente. Recomendamos-lhe que crie :ref:`uma predefinição de pincel "
"personalizada a seu gosto <loading_saving_brushes>`."

#: ../../tutorials/eye_tracker.rst:152
msgid ""
"There are various tools for selecting color but most of them are not easily "
"usable since they require quite a high level of mouse control. The Python "
"Palette Docker is the simplest to use where you select from a set of "
"predefined colors, similar to brush presets. Again, similarly to brush "
"selection, it will help to create a :ref:`custom set of favorite colors "
"<palette_docker>`."
msgstr ""
"Existem várias ferramentas para seleccionar cores, mas a maioria deles não é "
"fácil de usar por necessitarem de um grande nível de controlo do rato. A "
"Área da Paleta em Python é a mas simples de usar, na qual poderá seleccionar "
"a partir de um conjunto de cores predefinidas, de forma semelhante às "
"predefinições de pincéis. Mais uma vez, de forma semelhante à selecção de "
"pincéis, irá ajudar a criar um :ref:`conjunto personalizado de cores "
"favoritas <palette_docker>`."

#: ../../tutorials/eye_tracker.rst:158
msgid ""
"Once you are happy with your layout, another feature that will help you is "
"to lock the dockers. It's possible to accidentally close or move dockers. "
"For example, in drag and drop mode you can accidentally grab a docker and "
"drag it across the screen. To prevent this, put the following setting in "
"the :file:`kritarc` file::"
msgstr ""
"Assim que estiver satisfeito com a sua disposição, outra funcionalidade que "
"o irá ajudar é bloquear as áreas acopláveis. É possível fechar ou mover as "
"áreas sem querer. Por exemplo, no modo de arrastamento, poderá pegar sem "
"querer numa área acoplável e arrastá-la pelo ecrã. Para evitar isto, coloque "
"a seguinte opção no ficheiro :file:`kritarc`::"

#: ../../tutorials/eye_tracker.rst:164
msgid "LockAllDockerPanels=true"
msgstr "LockAllDockerPanels=true"

#: ../../tutorials/eye_tracker.rst:166
msgid ""
"(Check the :ref:`KritaFAQ` for how to find the configuration kritarc file on "
"your system.)"
msgstr ""
"(Consulte o :ref:`KritaFAQ` para saber como encontrar o ficheiro de "
"configuração 'kritarc' no seu sistema.)"

#: ../../tutorials/eye_tracker.rst:168
msgid ""
"If you're using a hovering mouse control menu like we do, you also have to "
"figure out where to place it when it's collapsed. Put it somewhere where it "
"will be easily accessible but where it will not interfere with Krita. On the "
"screenshot you can see it at the left edge of the screen."
msgstr ""
"Se estiver a usar um menu de controlo do rato à passagem como nós, poderá "
"também ter de perceber onde o colocar assim que o fechar. Coloque-o onde "
"fique facilmente acessível, mas onde não interfira com o Krita. Na imagem, "
"poderá ver o mesmo no extremo esquerdo do ecrã."

#: ../../tutorials/eye_tracker.rst:174
msgid "Summary"
msgstr "Resumo"

#: ../../tutorials/eye_tracker.rst:176
msgid "In summary, we work as explained below."
msgstr "Em resumo, funcionamos da forma explicada abaixo."

#: ../../tutorials/eye_tracker.rst:178
msgid "To start Krita:"
msgstr "Para iniciar o Krita:"

#: ../../tutorials/eye_tracker.rst:180
msgid ""
"On the desktop, pull up the hovering mouse menu and select left double click "
"mode."
msgstr ""
"No ecrã, invoque o menu à passagem do rato e seleccione o modo de duplo-"
"click com o botão esquerdo."

#: ../../tutorials/eye_tracker.rst:182
msgid ""
"Double click on the new drawing creation script. Look away at some harmless "
"spot until Krita loads."
msgstr ""
"Faça duplo-click sobre o programa de criação do novo desenho. Procure por "
"algum ponto seguro até que o Krita carregue."

#: ../../tutorials/eye_tracker.rst:185
msgid "Drawing with Krita:"
msgstr "Desenhar com o Krita:"

#: ../../tutorials/eye_tracker.rst:187 ../../tutorials/eye_tracker.rst:201
msgid "Switch to left single click mode."
msgstr "Mude para o modo de 'click' simples do botão esquerdo."

#: ../../tutorials/eye_tracker.rst:188
msgid "Select a brush and/or color using the dockers."
msgstr "Seleccione um pincel e/ou cor com as áreas acopláveis."

#: ../../tutorials/eye_tracker.rst:189
msgid "Switch to drag and drop mode. You're ready to draw."
msgstr "Mude para o modo de arrastamento. Está pronto para desenhar."

#: ../../tutorials/eye_tracker.rst:190
msgid ""
"Go to the point where you want to start a stroke and dwell until dragging "
"starts (this emulates pressing and holding your finger on the mouse button)."
msgstr ""
"Vá para o ponto onde deseja iniciar um traço e deslize até que comece o "
"arrastamento (isto emula o carregar e manter pressionado o dedo no botão do "
"rato)."

#: ../../tutorials/eye_tracker.rst:193
msgid "Draw."
msgstr "Desenhe."

#: ../../tutorials/eye_tracker.rst:194
msgid ""
"When you want to finish the current stroke, dwell at the ending point until "
"you get out of dragging (this emulates lifting your finger from the mouse "
"button)."
msgstr ""
"Quando quiser terminar o traço actual, desloque-se até ao ponto final até "
"que saia do arrastamento (isto emula o levantar do dedo do botão do rato)."

#: ../../tutorials/eye_tracker.rst:197
msgid "Repeat the whole process."
msgstr "Repita o processo completo."

#: ../../tutorials/eye_tracker.rst:199
msgid "Finishing:"
msgstr "Terminar:"

#: ../../tutorials/eye_tracker.rst:202
msgid "Click on the button for closing the window."
msgstr "Carregue no botão para fechar a janela."

#: ../../tutorials/eye_tracker.rst:203
msgid ""
"When warned about unsaved changes, click the button for saving the file."
msgstr ""
"Quando for avisado das alterações sem gravar, carregue no botão para gravar "
"o ficheiro."
