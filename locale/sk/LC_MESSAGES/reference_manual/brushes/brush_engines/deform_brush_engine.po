# translation of docs_krita_org_reference_manual___brushes___brush_engines___deform_brush_engine.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___deform_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-04-02 09:23+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Use Undeformed Image"
msgstr "Použiť nedeformovaný obrázok"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:1
msgid "The Deform Brush Engine manual page."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:16
msgid "Deform Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
#, fuzzy
#| msgid "Deform Amount"
msgid "Deform"
msgstr "Množstvo deformácie"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
msgid "Liquify"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:19
msgid ".. image:: images/icons/deformbrush.svg"
msgstr ".. image:: images/icons/deformbrush.svg"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:20
msgid ""
"The Deform Brush is a brush that allows you to pull and push pixels around. "
"It's quite similar to the :ref:`liquify_mode`, but where liquify has higher "
"quality, the deform brush has the speed."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:24
msgid "Options"
msgstr "Voľby"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:26
msgid ":ref:`option_brush_tip`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:27
msgid ":ref:`option_deform`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:28
msgid ":ref:`blending_modes`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:29
msgid ":ref:`option_opacity_n_flow`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:30
msgid ":ref:`option_size`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:31
msgid ":ref:`option_rotation`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:32
msgid ":ref:`option_airbrush`"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:38
msgid "Deform Options"
msgstr "Možnosti deformácie"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:42
#, fuzzy
#| msgid ".. image:: images/en/Krita_deform_brush_examples.png"
msgid ".. image:: images/brushes/Krita_deform_brush_examples.png"
msgstr ".. image:: images/en/Krita_deform_brush_examples.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:42
msgid ""
"1: undeformed, 2: Move, 3: Grow, 4: Shrink, 5: Swirl Counter Clock Wise, 6: "
"Swirl Clockwise, 7: Lens Zoom In, 8: Lens Zoom Out"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:44
msgid "These decide what strangeness may happen underneath your brush cursor."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:46
msgid "Grow"
msgstr "Zväčšiť"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:47
msgid "This bubbles up the area underneath the brush-cursor."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:48
msgid "Shrink"
msgstr "Zmenšiť"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:49
msgid "This pinches the Area underneath the brush-cursor."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:50
msgid "Swirl Counter Clock Wise"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:51
msgid "Swirls the area counter clock wise."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:52
msgid "Swirl Clock Wise"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:53
msgid "Swirls the area clockwise."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:54
msgid "Move"
msgstr "Presunúť"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:55
msgid "Nudges the area to the painting direction."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:56
msgid "Color Deformation"
msgstr "Deformácia farby"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:57
msgid "This seems to randomly rearrange the pixels underneath the brush."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:58
msgid "Lens Zoom In"
msgstr "Priblížiť šošovku"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:59
msgid "Literally paints a enlarged version of the area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:61
msgid "Lens Zoom Out"
msgstr "Oddialiť šošovku"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:61
msgid "Paints a minimized version of the area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:65
#, fuzzy
#| msgid ".. image:: images/en/Krita_deform_brush_colordeform.png"
msgid ".. image:: images/brushes/Krita_deform_brush_colordeform.png"
msgstr ".. image:: images/en/Krita_deform_brush_colordeform.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:65
msgid "Showing color deform."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:68
msgid "Deform Amount"
msgstr "Množstvo deformácie"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:68
msgid "Defines the strength of the deformation."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:72
#, fuzzy
#| msgid ".. image:: images/en/Krita_deform_brush_bilinear.png"
msgid ".. image:: images/brushes/Krita_deform_brush_bilinear.png"
msgstr ".. image:: images/en/Krita_deform_brush_bilinear.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:72
#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:74
msgid "Bilinear Interpolation"
msgstr "Bilineárna interpolácia"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:75
msgid "Smoothens the result. This causes calculation errors in 16bit."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:77
msgid "Use Counter"
msgstr "Použiť počítadlo"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:77
msgid "Slows down the deformation subtlety."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:81
#, fuzzy
#| msgid ".. image:: images/en/Krita_deform_brush_useundeformed.png"
msgid ".. image:: images/brushes/Krita_deform_brush_useundeformed.png"
msgstr ".. image:: images/en/Krita_deform_brush_useundeformed.png"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:81
msgid "Without 'use undeformed' to the left and with to the right."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:84
msgid ""
"Samples from the previous version of the image instead of the current. This "
"works better with some deform options than others. Move for example seems to "
"almost stop working, but it works really well with Grow."
msgstr ""
