# translation of docs_krita_org_reference_manual___dockers___task_sets.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___dockers___task_sets\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 10:14+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/dockers/task_sets.rst:1
msgid "Overview of the task sets docker."
msgstr ""

#: ../../reference_manual/dockers/task_sets.rst:16
msgid "Task Sets Docker"
msgstr "Panel sady úloh"

#: ../../reference_manual/dockers/task_sets.rst:18
msgid ""
"Task sets are for sharing a set of steps, like a tutorial. You make them "
"with the task-set docker."
msgstr ""

#: ../../reference_manual/dockers/task_sets.rst:21
#, fuzzy
#| msgid ".. image:: images/en/Task-set.png"
msgid ".. image:: images/dockers/Task-set.png"
msgstr ".. image:: images/en/Task-set.png"

#: ../../reference_manual/dockers/task_sets.rst:22
msgid ""
"Task sets can record any kind of command also available via the shortcut "
"manager. It can not record strokes, like the macro recorder can. However, "
"you can play macros with the tasksets!"
msgstr ""

#: ../../reference_manual/dockers/task_sets.rst:24
msgid ""
"The tasksets docker has a record button, and you can use this to record a "
"certain workflow. Then use this to let items appear in the taskset list. "
"Afterwards, turn off the record. You can then click any action in the list "
"to make them happen. Press the 'Save' icon to name and save the taskset."
msgstr ""
