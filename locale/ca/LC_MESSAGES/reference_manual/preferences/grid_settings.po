# Translation of docs_krita_org_reference_manual___preferences___grid_settings.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-21 03:28+0200\n"
"PO-Revision-Date: 2019-06-21 15:14+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../<generated>:1
msgid "Subdivision"
msgstr "Subdivisió"

#: ../../reference_manual/preferences/grid_settings.rst:1
msgid "Grid settings in Krita."
msgstr "Ajustaments per a la quadrícula al Krita."

#: ../../reference_manual/preferences/grid_settings.rst:15
msgid "Grid Settings"
msgstr "Ajustaments per a la quadrícula"

#: ../../reference_manual/preferences/grid_settings.rst:19
msgid "Deprecated in 3.0, use the :ref:`grids_and_guides_docker` instead."
msgstr ""
"Obsoleta des de la versió 3.0, en el seu lloc utilitzeu l':ref:"
"`grids_and_guides_docker`."

#: ../../reference_manual/preferences/grid_settings.rst:21
msgid "Use :menuselection:`Settings --> Configure Krita --> Grid` menu item."
msgstr ""
"Utilitzeu l'element de menú :menuselection:`Arranjament --> Configura el "
"Krita --> Quadrícula`."

#: ../../reference_manual/preferences/grid_settings.rst:24
msgid ".. image:: images/preferences/Krita_Preferences_Grid.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Grid.png"

#: ../../reference_manual/preferences/grid_settings.rst:25
msgid "Fine tune the settings of the grid-tool grid here."
msgstr ""
"Ajust aquí dels ajustaments de la quadrícula per a l'eina de quadrícula."

#: ../../reference_manual/preferences/grid_settings.rst:28
msgid "Placement"
msgstr "Ubicació"

#: ../../reference_manual/preferences/grid_settings.rst:30
msgid "The user can set various settings of the grid over here."
msgstr "L'usuari pot establir aquí diversos ajustaments de la quadrícula."

#: ../../reference_manual/preferences/grid_settings.rst:32
msgid "Horizontal Spacing"
msgstr "Espaiat horitzontal"

#: ../../reference_manual/preferences/grid_settings.rst:33
msgid ""
"The number in Krita units, the grid will be spaced in the horizontal "
"direction."
msgstr ""
"El nombre en unitats del Krita, la quadrícula s'espaiarà en la direcció "
"horitzontal."

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid "Vertical Spacing"
msgstr "Espaiat vertical"

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid ""
"The number in Krita units, the grid will be spaced in the vertical "
"direction. The images below will show the usage of these settings."
msgstr ""
"El nombre en unitats del Krita, la quadrícula s'espaiarà en la direcció "
"vertical. Les imatges a continuació mostren l'ús d'aquests ajustaments."

#: ../../reference_manual/preferences/grid_settings.rst:37
msgid "X Offset"
msgstr "Desplaçament X"

#: ../../reference_manual/preferences/grid_settings.rst:38
msgid "The number to offset the grid in the X direction."
msgstr "El número per a desplaçar la quadrícula en la direcció X."

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "Y Offset"
msgstr "Desplaçament Y"

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "The number to offset the grid in the Y direction."
msgstr "El número per a desplaçar la quadrícula en la direcció Y."

#: ../../reference_manual/preferences/grid_settings.rst:42
msgid ""
"Some examples are shown below, look at the edge of the image to see the "
"offset."
msgstr ""
"A continuació es mostren alguns exemples, mireu la vora de la imatge per a "
"veure el desplaçament."

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid "Subdivisions"
msgstr "Subdivisions"

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid ""
"Here the user can set the number of times the grid is subdivided. Some "
"examples are shown below."
msgstr ""
"Aquí l'usuari podrà establir el nombre de vegades que estarà subdividida la "
"quadrícula. A continuació es mostren alguns exemples."

#: ../../reference_manual/preferences/grid_settings.rst:48
msgid "Style"
msgstr "Estil"

#: ../../reference_manual/preferences/grid_settings.rst:50
msgid "Main"
msgstr "Principal"

#: ../../reference_manual/preferences/grid_settings.rst:51
msgid ""
"The user can set how the main lines of the grid are shown. Options available "
"are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""
"L'usuari podrà establir com es mostraran les línies principals de la "
"quadrícula. Les opcions disponibles són Línies, Línies discontínues, Punts. "
"El color també es podrà establir aquí."

#: ../../reference_manual/preferences/grid_settings.rst:53
msgid ""
"The user can set how the subdivision lines of the grid are shown. Options "
"available are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""
"L'usuari podrà establir com es mostraran les línies de subdivisió de la "
"quadrícula. Les opcions disponibles són Línies, Línies discontínues, Punts. "
"El color també es podrà establir aquí."
