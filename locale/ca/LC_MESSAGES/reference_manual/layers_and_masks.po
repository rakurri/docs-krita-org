# Translation of docs_krita_org_reference_manual___layers_and_masks.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-02 17:59+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/layers_and_masks.rst:5
msgid "Layers and Masks"
msgstr "Capes i màscares"

#: ../../reference_manual/layers_and_masks.rst:7
msgid "Layers are a central concept in digital painting."
msgstr "Les capes són un concepte central en la pintura digital."

#: ../../reference_manual/layers_and_masks.rst:9
msgid ""
"With layers you can get better control over your artwork, for example you "
"can color an entire artwork just by working on the separate color layer and "
"thereby not destroying the line art which will reside above this color layer."
msgstr ""
"Amb les capes obtindreu un millor control sobre el vostre treball artístic, "
"per exemple, podreu donar color a tot un treball artístic simplement "
"treballant sobre la capa de color separada i, per tant, no destruint la "
"línia artística que es troba sobre aquesta capa de color."

#: ../../reference_manual/layers_and_masks.rst:11
msgid ""
"Furthermore, layers allow you to change the composition easier, and mass "
"transform certain elements at once."
msgstr ""
"A més, les capes permeten canviar la composició amb més facilitat i "
"transformar certs elements alhora."

#: ../../reference_manual/layers_and_masks.rst:13
msgid ""
"Masks on the other hand allow you to selectively apply certain effects on a "
"layer, like transparency, transformation and filters."
msgstr ""
"D'altra banda, les màscares permeten aplicar selectivament certs efectes "
"sobre una capa, com la transparència, la transformació i els filtres."

#: ../../reference_manual/layers_and_masks.rst:15
msgid "Check the :ref:`layers_and_masks` for more information."
msgstr ""
"Consulteu la :ref:`layers_and_masks` en el manual d'usuari per obtenir més "
"informació."
