# Translation of docs_krita_org_user_manual___snapping.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_user_manual___snapping\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:51+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../user_manual/snapping.rst:1
msgid "How to use the snapping functionality in Krita."
msgstr "Як користуватися можливостями прилипання у Krita."

#: ../../user_manual/snapping.rst:10 ../../user_manual/snapping.rst:43
msgid "Guides"
msgstr "Напрямні"

#: ../../user_manual/snapping.rst:10
msgid "Snap"
msgstr "Прилипання"

#: ../../user_manual/snapping.rst:10
msgid "Vector"
msgstr "Вектор"

#: ../../user_manual/snapping.rst:15
msgid "Snapping"
msgstr "Прилипання"

#: ../../user_manual/snapping.rst:17
msgid ""
"In Krita 3.0, we now have functionality for Grids and Guides, but of course, "
"this functionality is by itself not that interesting without snapping."
msgstr ""
"Починаючи з Krita 3.0, у програмі реалізовано ґратки та напрямні, але, "
"звичайно ж, ці функціональні можливості не дуже корисні без функціональної "
"можливості прилипання."

#: ../../user_manual/snapping.rst:21
msgid ""
"Snapping is the ability to have Krita automatically align a selection or "
"shape to the grids and guides, document center and document edges. For "
"Vector layers, this goes even a step further, and we can let you snap to "
"bounding boxes, intersections, extrapolated lines and more."
msgstr ""
"Прилипання — можливість у Krita автоматично вирівнювати позначену область "
"або форму за ґратками та напрямними, центром чи краями документа. Для "
"векторних шарів передбачено ще ширші можливості прилипання: до обмежувальних "
"рамок, перетинів, продовження ліній тощо."

#: ../../user_manual/snapping.rst:26
msgid ""
"All of these can be toggled using the snap pop-up menu which is assigned to :"
"kbd:`Shift + S` shortcut."
msgstr ""
"Усі параметри прилипання можна змінити за допомогою контекстного меню "
"прилипання, яке можна викликати натисканням комбінації клавіш :kbd:`Shift+S`."

#: ../../user_manual/snapping.rst:29
msgid "Now, let us go over what each option means:"
msgstr "Тепер розберемо кожен із параметрів окремо:"

#: ../../user_manual/snapping.rst:32
msgid ""
"This will snap the cursor to the current grid, as configured in the grid "
"docker. This doesn’t need the grid to be visible. Grids are saved per "
"document, making this useful for aligning your art work to grids, as is the "
"case for game sprites and grid-based designs."
msgstr ""
"Це прилипання вказівника до поточної ґратки, яку налаштовано за допомогою "
"бічної панелі ґратки. Ґратка не обов'язково має бути видимою. Ґратки "
"зберігаються на рівні документів. Прилипання до ґратки — зручний інструмент "
"вирівнювання вашого художнього твору за ґраткою, зокрема у випадку спрайтів "
"для ігор або компонувань малюнків, заснованих на ґратці."

#: ../../user_manual/snapping.rst:34
msgid "Grids"
msgstr "Сітки"

#: ../../user_manual/snapping.rst:37
msgid "Pixel"
msgstr "Піксель"

#: ../../user_manual/snapping.rst:37
msgid ""
"This allows to snap to every pixel under the cursor. Similar to Grid "
"Snapping but with a grid having spacing = 1px and offset = 0px."
msgstr ""
"Це прилипання до будь-якого пікселя під курсором. Подібне до прилипання до "
"ґратки, але із інтервалом ґратки = 1 піксель і відступом = 0 пікселів."

#: ../../user_manual/snapping.rst:40
msgid ""
"This allows you to snap to guides, which can be dragged out from the ruler. "
"Guides do not need to be visible for this, and are saved per document. This "
"is useful for comic panels and similar print-layouts, though we recommend "
"Scribus for more intensive work."
msgstr ""
"Це прилипання до напрямних, які можна перетягувати з лінійок. Для прилипання "
"напрямні не обов'язково мають бути видимими. Дані напрямних зберігаються у "
"документі. Таке прилипання є корисним для панелей коміксів або подібних "
"призначених для друку компонувань, хоча для інтенсивної роботи з "
"приготування матеріалів для друку ми рекомендуємо скористатися Scribus."

#: ../../user_manual/snapping.rst:46
msgid ".. image:: images/snapping/Snap-orthogonal.png"
msgstr ".. image:: images/snapping/Snap-orthogonal.png"

#: ../../user_manual/snapping.rst:48
msgid ""
"This allows you to snap to a horizontal or vertical line from existing "
"vector objects’s nodes (Unless dealing with resizing the height or width "
"only, in which case you can drag the cursor over the path). This is useful "
"for aligning object horizontally or vertically, like with comic panels."
msgstr ""
"Це прилипання до горизонтальної або вертикальної лінії з наявних вузлів "
"векторних об'єктів (окрім зміни розмірів лише за висотою або лише за "
"шириною, коли ви можете перетягувати вказівник за контуром). Корисно для "
"вертикального або горизонтального вирівнювання об'єктів, зокрема на панелях "
"коміксів."

#: ../../user_manual/snapping.rst:52
msgid "Orthogonal (Vector Only)"
msgstr "Ортогональне (лише векторні зображення)"

#: ../../user_manual/snapping.rst:55
msgid ".. image:: images/snapping/Snap-node.png"
msgstr ".. image:: images/snapping/Snap-node.png"

#: ../../user_manual/snapping.rst:57
msgid "Node (Vector Only)"
msgstr "Вузол (лише векторні зображення)"

#: ../../user_manual/snapping.rst:57
msgid "This snaps a vector node or an object to the nodes of another path."
msgstr "Це прилипання векторного вузла або об'єкта до вузлів іншого контуру."

#: ../../user_manual/snapping.rst:60
msgid ".. image:: images/snapping/Snap-extension.png"
msgstr ".. image:: images/snapping/Snap-extension.png"

#: ../../user_manual/snapping.rst:62
msgid ""
"When we draw an open path, the last nodes on either side can be "
"mathematically extended. This option allows you to snap to that. The "
"direction of the node depends on its side handles in path editing mode."
msgstr ""
"Коли ви малюєте незамкнений контур, лінію контуру можна математично "
"продовжити за останній і перший вузол цього контуру. За допомогою цього "
"варіанта прилипання можна скористатися прилипанням до таких продовжень. "
"Напрям для вузла залежить від його бічних елементів керування у режимі "
"редагування контуру."

#: ../../user_manual/snapping.rst:65
msgid "Extension (Vector Only)"
msgstr "Розширення (лише векторні зображення)"

#: ../../user_manual/snapping.rst:68
msgid ".. image:: images/snapping/Snap-intersection.png"
msgstr ".. image:: images/snapping/Snap-intersection.png"

#: ../../user_manual/snapping.rst:69
msgid "Intersection (Vector Only)"
msgstr "Перетин (лише векторні зображення)"

#: ../../user_manual/snapping.rst:70
msgid "This allows you to snap to an intersection of two vectors."
msgstr "Уможливлює прилипання до будь-якого перетину двох векторних ліній."

#: ../../user_manual/snapping.rst:71
msgid "Bounding box (Vector Only)"
msgstr "Обмежувальна рамка (лише векторні зображення)"

#: ../../user_manual/snapping.rst:72
msgid "This allows you to snap to the bounding box of a vector shape."
msgstr "Уможливлює прилипання до обмежувальної рамки векторної форми."

#: ../../user_manual/snapping.rst:74
msgid "Image bounds"
msgstr "Межі зображення"

#: ../../user_manual/snapping.rst:74
msgid "Allows you to snap to the vertical and horizontal borders of an image."
msgstr ""
"Уможливлює прилипання до вертикальних або горизонтальних меж зображення."

#: ../../user_manual/snapping.rst:77
msgid "Allows you to snap to the horizontal and vertical center of an image."
msgstr ""
"Уможливлює прилипання до центру зображення за вертикаллю і горизонталлю."

#: ../../user_manual/snapping.rst:78
msgid "Image center"
msgstr "Центр зображення"

#: ../../user_manual/snapping.rst:80
msgid "The snap works for the following tools:"
msgstr "Прилипання працює для таких інструментів:"

#: ../../user_manual/snapping.rst:82
msgid "Straight line"
msgstr "Пряма"

#: ../../user_manual/snapping.rst:83
msgid "Rectangle"
msgstr "Прямокутник"

#: ../../user_manual/snapping.rst:84
msgid "Ellipse"
msgstr "Еліпс"

#: ../../user_manual/snapping.rst:85
msgid "Polyline"
msgstr "Ламана"

#: ../../user_manual/snapping.rst:86
msgid "Path"
msgstr "Контур"

#: ../../user_manual/snapping.rst:87
msgid "Freehand path"
msgstr "Довільний контур"

#: ../../user_manual/snapping.rst:88
msgid "Polygon"
msgstr "Багатокутник"

#: ../../user_manual/snapping.rst:89
msgid "Gradient"
msgstr "Градієнт"

#: ../../user_manual/snapping.rst:90
msgid "Shape Handling tool"
msgstr "Інструмент обробки форм"

#: ../../user_manual/snapping.rst:91
msgid "The Text-tool"
msgstr "Інструмент тексту"

#: ../../user_manual/snapping.rst:92
msgid "Assistant editing tools"
msgstr "Інструменти допоміжного редагування"

#: ../../user_manual/snapping.rst:93
msgid ""
"The move tool (note that it snaps to the cursor position and not the "
"bounding box of the layer, selection or whatever you are trying to move)"
msgstr ""
"Інструмент пересування (зауважте, що він прилипає до позиції вказівника, а "
"не до обмежувальної рамки шару, позначення або того об'єкта, який ви "
"намагаєтеся пересунути)"

#: ../../user_manual/snapping.rst:96
msgid "The Transform tool"
msgstr "Інструмент перетворення"

#: ../../user_manual/snapping.rst:97
msgid "Rectangle select"
msgstr "Прямокутний вибір"

#: ../../user_manual/snapping.rst:98
msgid "Elliptical select"
msgstr "Еліптичний вибір"

#: ../../user_manual/snapping.rst:99
msgid "Polygonal select"
msgstr "Вибір багатокутником"

#: ../../user_manual/snapping.rst:100
msgid "Path select"
msgstr "Вибір контуром"

#: ../../user_manual/snapping.rst:101
msgid "Guides themselves can be snapped to grids and vectors"
msgstr "Напрямні лінії самі можуть прилипати до ґратки і векторних ліній."

#: ../../user_manual/snapping.rst:103
msgid ""
"Snapping doesn’t have a sensitivity yet, and by default is set to 10 screen "
"pixels."
msgstr ""
"Засобів налаштовування відстані для прилипання ще не передбачено. Типово, "
"прилипання відбувається, якщо відстань стає меншою за 10 пікселів."
