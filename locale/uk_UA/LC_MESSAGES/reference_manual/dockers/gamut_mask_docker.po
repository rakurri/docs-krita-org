# Translation of docs_krita_org_reference_manual___dockers___gamut_mask_docker.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___gamut_mask_docker\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 08:25+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:1
msgid "Overview of the gamut mask docker."
msgstr "Огляд бічної панелі маски діапазону кольорів."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Color"
msgstr "Колір"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Color Selector"
msgstr "Вибір кольору"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Gamut Mask Docker"
msgstr "Бічна панель маски діапазону кольорів"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:11
msgid "Gamut Mask"
msgstr "Маска діапазону кольорів"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:16
msgid "Gamut Masks Docker"
msgstr "Бічна панель масок діапазону кольорів"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:19
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:22
msgid "Docker for gamut masks selection and management."
msgstr "Бічна панель для вибору масок діапазону кольорів і керування ними."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:25
msgid "Usage"
msgstr "Користування"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:27
msgid "|mouseleft| an icon (1) to apply a mask to color selectors."
msgstr ""
"Клацніть |mouseleft| на піктограмі (1), щоб застосувати маску до засобів "
"вибору кольору."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:29
msgid "Gamut Masks can be imported and exported in the resource manager."
msgstr ""
"Маски діапазонів кольорів можна імпортувати та експортувати за допомогою "
"засобу керування ресурсами."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:33
msgid "Management Toolbar"
msgstr "Панель інструментів керування"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:35
msgid "Create new mask (2)"
msgstr "Створити маску (2)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:36
msgid "Opens the mask editor with an empty template."
msgstr "Відкриває редактор масок із порожнім шаблоном."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:37
msgid "Edit mask (3)"
msgstr "Змінити маску (3)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:38
msgid "Opens the the currently selected mask in the editor."
msgstr "Відкриває поточну позначену маску у редакторі."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:39
msgid "Duplicate mask (4)"
msgstr "Дублювати маску (4)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:40
msgid ""
"Creates a copy of the currently selected mask and opens the copy in the "
"editor."
msgstr ""
"Створює копію поточної позначеної маски і відкриває цю копію у редакторі."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:42
msgid "Deletes the currently selected mask."
msgstr "Вилучає поточну позначену маску."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:43
msgid "Delete mask (5)"
msgstr "Вилучити маску (5)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:46
msgid "Editing"
msgstr "Редагування"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:48
msgid ""
"If you choose to create a new mask, edit, or duplicate selected mask, the "
"mask template document will be opened as a new view (1)."
msgstr ""
"Якщо ви вибрали пункт створення маски, редагування маски або дублювання "
"позначеної маски, документи шаблонів масок буде відкрито на новій панелі "
"перегляду (1)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:50
msgid ""
"There you can create new shapes and modify the mask with standard vector "
"tools (:ref:`vector_graphics`)."
msgstr ""
"Тут ви зможете створити нові форми і змінити маску за допомогою стандартних "
"інструментів редагування векторних форм (:ref:`vector_graphics`)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:52
msgid "Fill in the fields at (2)."
msgstr "Заповнити поля у (2)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:54
msgid "Title (Mandatory)"
msgstr "Заголовок (обов'язковий)"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:55
msgid "The name of the gamut mask."
msgstr "Назва маски діапазону кольорів."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:57
msgid "Description"
msgstr "Опис"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:57
msgid "A description."
msgstr "Опис."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:59
msgid ""
":guilabel:`Preview` the mask in the artistic color selector (4), :guilabel:"
"`save` the mask (5), or :guilabel:`cancel` editing (3)."
msgstr ""
":guilabel:`Переглянути` маску на панелі художнього кольору (4), :guilabel:"
"`Зберегти` маску (5) або :guilabel:`Скасувати` редагування (3)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:63
msgid ""
"The shapes need to be added to the layer named “maskShapesLayer” (which is "
"selected by default)."
msgstr ""
"Форми слід додавати на шар із назвою “maskShapesLayer” (який буде типово "
"позначено)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:64
msgid "The shapes need have solid background to show correctly in the editor."
msgstr ""
"Щоб форми належним чином було показано у редакторі, їхнє тло має бути "
"суцільним (без прозорості)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:65
msgid "A template with no shapes cannot be saved."
msgstr "Шаблон без масок зберегти неможливо."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:69
msgid ""
"The mask is intended to be composed of basic vector shapes. Although "
"interesting results might arise from using advanced vector drawing "
"techniques, not all features are guaranteed to work properly (e.g. grouping, "
"vector text, etc.)."
msgstr ""
"Маску має бути складено з базових векторних форм. Хоча цікаві результати "
"можна отримати використанням розширених методик малювання векторних форм, "
"розробники програми не гарантують можливості належним чином скористатися "
"усіма варіантами векторних форм (наприклад групуванням, векторним текстом "
"тощо)."

#: ../../reference_manual/dockers/gamut_mask_docker.rst:72
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:74
msgid "External Info"
msgstr "Сторонні відомості"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:76
msgid ""
"`Color Wheel Masking, Part 1 by James Gurney. <https://gurneyjourney."
"blogspot.com/2008/01/color-wheel-masking-part-1.html>`_"
msgstr ""
"`Маскування за кольоровим колом, частина 1, Джеймс Ґурні (James Gurney) "
"<https://gurneyjourney.blogspot.com/2008/01/color-wheel-masking-part-1."
"html>`_"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:77
msgid ""
"`The Shapes of Color Schemes by James Gurney. <https://gurneyjourney."
"blogspot.com/2008/02/shapes-of-color-schemes.html>`_"
msgstr ""
"`Форми схем кольорів, Джеймс Ґурні (James Gurney) <https://gurneyjourney."
"blogspot.com/2008/02/shapes-of-color-schemes.html>`_"

#: ../../reference_manual/dockers/gamut_mask_docker.rst:78
msgid ""
"`Gamut Masking Demonstration by James Gourney (YouTube). <https://youtu.be/"
"qfE4E5goEIc>`_"
msgstr ""
"`Демонстрація маскування за діапазоном кольорів, яку створено Джеймсом Гурні "
"(James Gourney, YouTube) <https://youtu.be/qfE4E5goEIc>`_"
