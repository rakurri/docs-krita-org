# Translation of docs_krita_org_reference_manual___tools___similar_select.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___similar_select\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:36+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Fuzziness"
msgstr "Нечіткість"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../<rst_epilog>:78
msgid ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"
msgstr ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"

#: ../../reference_manual/tools/similar_select.rst:1
msgid "Krita's similar color selection tool reference."
msgstr "Довідник із інструмента позначення подібного Krita."

#: ../../reference_manual/tools/similar_select.rst:11
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/similar_select.rst:11
msgid "Selection"
msgstr "Позначення"

#: ../../reference_manual/tools/similar_select.rst:11
msgid "Similar Selection"
msgstr "Позначення подібного"

#: ../../reference_manual/tools/similar_select.rst:16
msgid "Similar Color Selection Tool"
msgstr "Інструмент «Вибір подібного кольору»"

#: ../../reference_manual/tools/similar_select.rst:18
msgid "|toolselectsimilar|"
msgstr "|toolselectsimilar|"

#: ../../reference_manual/tools/similar_select.rst:20
msgid ""
"This tool, represented by a dropper over an area with a dashed border, "
"allows you to make :ref:`selections_basics` by selecting a point of color. "
"It will select any areas of a similar color to the one you selected. You can "
"adjust the \"fuzziness\" of the tool in the tool options dock. A lower "
"number will select colors closer to the color that you chose in the first "
"place."
msgstr ""
"За допомогою цього інструмента, на кнопці якого показано піпетку на тлі "
"ділянки зображення, обмеженої пунктиром, ви можете виконувати :ref:"
"`selections_basics`, вказавши точку певного кольору. Інструмент позначає "
"будь-які області, які зафарбовано кольором, подібним до вибраного. "
"Скоригувати :guilabel:`Нечіткість` роботи інструмента можна за допомогою "
"бічної панелі параметрів інструментів. Менше значення призведе до позначення "
"областей, які зафарбовано кольорами, які є ближчими до вибраного."

#: ../../reference_manual/tools/similar_select.rst:23
msgid "Hotkeys and Sticky keys"
msgstr "Керування за допомогою клавіатури"

#: ../../reference_manual/tools/similar_select.rst:25
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` встановлює параметри позначення у режим «замінити»; це типовий "
"режим."

#: ../../reference_manual/tools/similar_select.rst:26
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ""
":kbd:`A` встановлює для позначення режим «додавання» у параметрах "
"інструмента."

#: ../../reference_manual/tools/similar_select.rst:27
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""
":kbd:`S` встановлює для позначення режим «віднімання» у параметрах "
"інструмента."

#: ../../reference_manual/tools/similar_select.rst:28
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
"За допомогою комбінації :kbd:`Shift` + |mouseleft| можна встановити для "
"наступного позначення області режим «додавання». Під час перетягування ви "
"можете відпустити клавішу :kbd:`Shift`, але режим лишатиметься режимом "
"«додавання». Те саме стосується і інших режимів."

#: ../../reference_manual/tools/similar_select.rst:29
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt` + |mouseleft| встановлює для наступного позначення режим "
"«віднімання»."

#: ../../reference_manual/tools/similar_select.rst:30
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl` + |mouseleft| встановлює для наступного позначення режим "
"«заміна»."

#: ../../reference_manual/tools/similar_select.rst:31
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Shift + Alt +` |mouseleft| встановлює для наступного позначення режим "
"«перетин»."

#: ../../reference_manual/tools/similar_select.rst:35
msgid "Hovering over a selection allows you to move it."
msgstr ""
"Наведення вказівника на позначену ділянку надасть вам змогу пересунути її."

#: ../../reference_manual/tools/similar_select.rst:36
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Клацання |mouseright| відкриває меню швидкого доступу для позначеної "
"ділянки, у якому, серед інших пунктів, є і пункт редагування позначеної "
"ділянки."

#: ../../reference_manual/tools/similar_select.rst:40
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Ви можете перемкнути поведінку програми у відповідь на натискання :kbd:`Alt` "
"так, щоб натискати замість цієї клавіші клавішу :kbd:`Ctrl`. Для цього "
"призначено спеціальний пункт на сторінці :ref:`general_settings`."

#: ../../reference_manual/tools/similar_select.rst:43
msgid "Tool Options"
msgstr "Параметри інструмента"

#: ../../reference_manual/tools/similar_select.rst:46
msgid ""
"This controls whether or not the contiguous selection sees another color as "
"a border."
msgstr ""
"Цей параметр визначає, чи розглядатиметься під час позначення неперервної "
"області інший колір як колір межі."
