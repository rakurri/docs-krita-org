# Translation of docs_krita_org_reference_manual___brushes___brush_settings___texture.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_settings___texture\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:35+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_05.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_05.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_04.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_04.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_01.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_02.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_07.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_07.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:0
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_06.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_06.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:1
msgid "The texture brush settings option in Krita."
msgstr "Параметри текстурного пензля у Krita."

#: ../../reference_manual/brushes/brush_settings/texture.rst:11
#: ../../reference_manual/brushes/brush_settings/texture.rst:16
#: ../../reference_manual/brushes/brush_settings/texture.rst:21
msgid "Texture"
msgstr "Текстура"

#: ../../reference_manual/brushes/brush_settings/texture.rst:11
msgid "Patterns"
msgstr "Візерунки"

#: ../../reference_manual/brushes/brush_settings/texture.rst:18
msgid ""
"This allows you to have textured strokes. This parameter always shows up as "
"two parameters:"
msgstr ""
"Уможливлює використання текстурних мазків. Можливості цього параметра "
"поділено між двома пунктами:"

#: ../../reference_manual/brushes/brush_settings/texture.rst:23
#: ../../reference_manual/brushes/brush_settings/texture.rst:46
msgid "Pattern"
msgstr "Візерунок"

#: ../../reference_manual/brushes/brush_settings/texture.rst:24
msgid "Which pattern you'll be using."
msgstr "Візерунок, яким ви хочете скористатися."

#: ../../reference_manual/brushes/brush_settings/texture.rst:26
msgid "The size of the pattern. 1.0 is 100%."
msgstr "Розмір візерунка. 1.0 — 100%."

#: ../../reference_manual/brushes/brush_settings/texture.rst:27
msgid "Scale"
msgstr "Масштаб"

#: ../../reference_manual/brushes/brush_settings/texture.rst:30
msgid "How much a brush is offset, random offset sets a new per stroke."
msgstr "Відступ пензля. Випадковий відступ змінюється кожного мазка."

#: ../../reference_manual/brushes/brush_settings/texture.rst:31
msgid "Horizontal Offset & Vertical Offset"
msgstr "Горизонтальний і вертикальний відступи"

#: ../../reference_manual/brushes/brush_settings/texture.rst:34
msgid "Multiply"
msgstr "Множення"

#: ../../reference_manual/brushes/brush_settings/texture.rst:35
msgid ""
"Uses alpha multiplication to determine the effect of the texture. Has a soft "
"feel."
msgstr ""
"Використовує множення з прозорістю для визначення впливу текстури. Пом'якшує "
"результат застосування текстури."

#: ../../reference_manual/brushes/brush_settings/texture.rst:37
msgid "Subtract"
msgstr "Віднімання"

#: ../../reference_manual/brushes/brush_settings/texture.rst:37
msgid ""
"Uses subtraction to determine the effect of the texture. Has a harsher, more "
"texture feel."
msgstr ""
"Використовує віднімання для визначення впливу текстури. Надає результату "
"шорсткішого, подібнішого до текстури вигляду."

#: ../../reference_manual/brushes/brush_settings/texture.rst:39
msgid "Texturing mode"
msgstr "Режим текстурування"

#: ../../reference_manual/brushes/brush_settings/texture.rst:42
msgid ""
"Cutoff policy will determine what range and where the strength will affect "
"the textured outcome."
msgstr ""
"Правила обрізання визначають, у якому діапазоні і де потужність ефекту буде "
"застосовано до текстурованих результатів обробки."

#: ../../reference_manual/brushes/brush_settings/texture.rst:44
msgid "Disabled"
msgstr "Вимкнено"

#: ../../reference_manual/brushes/brush_settings/texture.rst:45
msgid "Doesn't cut off. Full range will be used."
msgstr "Не обрізано. Буде використано увесь діапазон."

#: ../../reference_manual/brushes/brush_settings/texture.rst:47
msgid "Cuts the pattern off."
msgstr "Обрізає візерунок."

#: ../../reference_manual/brushes/brush_settings/texture.rst:49
msgid "Brush"
msgstr "Пензель"

#: ../../reference_manual/brushes/brush_settings/texture.rst:49
msgid "Cuts the brush-tip off."
msgstr "Обрізає кінчик пензля."

#: ../../reference_manual/brushes/brush_settings/texture.rst:51
msgid "Cutoff policy"
msgstr "Правила обрізання"

#: ../../reference_manual/brushes/brush_settings/texture.rst:54
msgid ""
"Cutoff is... the grayscale range that you can limit the texture to. This "
"also affects the limit takes by the strength. In the below example, we move "
"from the right arrow moved close to the left one, resulting in only the "
"darkest values being drawn. After that, three images with larger range, and "
"underneath that, three ranges with the left arrow moved, result in the "
"darkest values being cut away, leaving only the lightest. The last example "
"is the pattern without cutoff."
msgstr ""
"Обрізання — це… діапазон відтінків сірого кольору, яким можна обмежити "
"текстуру. Це також впливає на обмеження потужності. У наведеному нижче "
"прикладі ми бачимо результати зміни діапазону. Спочатку, права ліва стрілка "
"є близькою до лівої, отже ми бачимо лише найтемніші точки. Далі, маємо три "
"зображення із ширшим діапазоном, а під ними ще три, де ми пересували лише "
"ліву стрілку, обрізаючи темні пікселі і лишаючи лише найсвітліші. На "
"останньому малюнку бачимо візерунок без обрізання."

#: ../../reference_manual/brushes/brush_settings/texture.rst:56
msgid "Cutoff"
msgstr "Обрізання"

#: ../../reference_manual/brushes/brush_settings/texture.rst:59
msgid "Invert the pattern."
msgstr "Інвертувати візерунок."

#: ../../reference_manual/brushes/brush_settings/texture.rst:61
msgid "Invert Pattern"
msgstr "Інвертувати візерунок"

#: ../../reference_manual/brushes/brush_settings/texture.rst:63
msgid "Brightness and Contrast"
msgstr "Яскравість і контрастність"

#: ../../reference_manual/brushes/brush_settings/texture.rst:67
msgid ""
"Adjust the pattern with a simple brightness/contrast filter to make it "
"easier to use. Because Subtract and Multiply work differently, it's "
"recommended to use different values with each:"
msgstr ""
"Скоригуйте візерунок простим фільтром яскравості-контрастності для "
"полегшення його використання. Оскільки режими віднімання та множення "
"працюють у різний спосіб, рекомендуємо використовувати різні значення для "
"кожного з цих режимів:"

#: ../../reference_manual/brushes/brush_settings/texture.rst:69
msgid ".. image:: images/brushes/Krita_3_1_brushengine_texture_07.png"
msgstr ".. image:: images/brushes/Krita_3_1_brushengine_texture_07.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:72
msgid "Strength"
msgstr "Потужність"

#: ../../reference_manual/brushes/brush_settings/texture.rst:74
msgid ""
"This allows you to set the texture to Sensors. It will use the cutoff to "
"continuously draw lighter values of the texture (making the result darker)."
msgstr ""
"Цей параметр надає вам змогу встановити для текстури режим «Датчики». У "
"ньому використовуватиметься обрізання для неперервного малювання світліших "
"елементів текстури (ще зробить результат темнішим)."

#: ../../reference_manual/brushes/brush_settings/texture.rst:77
msgid ".. image:: images/brushes/Krita_2_9_brushengine_texture_03.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_texture_03.png"

#: ../../reference_manual/brushes/brush_settings/texture.rst:80
msgid ""
"`David Revoy describing the texture feature (old). <https://www.davidrevoy."
"com/article107/textured-brush-in-floss-digital-painting>`_"
msgstr ""
"`David Revoy описує можливість використання текстур (давнє відео) <https://"
"www.davidrevoy.com/article107/textured-brush-in-floss-digital-painting>`_"
